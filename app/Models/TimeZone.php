<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model
{
    protected $table = 'time_zone';
    protected $primaryKey ='id_time_zone';
    public $fillable =[
        'value_time_zone',
        'created_at',
        'updated_at'
    ];
}
