<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'promotion';
    protected $primaryKey ='id_promotion';
    public $fillable =[
        'discount',
        'date_promotion',
        'days_promo',
        'id_account'
    ];
}
