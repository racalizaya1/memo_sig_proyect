<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionRoom extends Model
{
    protected $table = 'promotion_room';
    protected $primaryKey ='id_promotion_room';
    public $fillable =[
        'id_promotion',
        'id_room'
    ];
}
