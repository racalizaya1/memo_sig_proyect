<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';
    protected $primaryKey ='id_notification';
    public $fillable =[
        'description',
        'viewed',
        'id_account',
        'id_room',
        'id_type_notification'
    ];
}
