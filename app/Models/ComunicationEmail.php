<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComunicationEmail extends Model
{
    protected $table = 'comunication_email';
    protected $primaryKey ='id_comunication_email';
    public $fillable =[
        'body_email',
        'date_email',
        'type_direction',
        'status_email',
        'id_room',
        'id_account',
        'id_user'
    ];
}
