<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Advertising extends Model
{
    protected $table = 'advertising';
    protected $primaryKey ='id_advertising';
    public $fillable =[
        'days_advertising',
        'active_adds',
        'id_room'
    ];
}
