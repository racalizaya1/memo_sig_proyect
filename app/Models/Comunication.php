<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comunication extends Model
{
    protected $table = 'comunication';
    protected $primaryKey ='id_comunication';
    public $fillable =[
        'name_account',
        'twilio_phone',
        'call_whisper',
        'call_whisper_delay',
        'voicemail',
        'voicemail_delay',
        'voicemail_file'
    ];
}
