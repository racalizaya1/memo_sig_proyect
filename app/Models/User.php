<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';
    protected $primaryKey ='id_user';
    public $fillable =[
        'first_name',
        'last_name',
        'email_user',
        'phone_user',
        'address_user',
        'zip_code',
        'username',
        'password',
        'active_user',
        'id_type_role',
        'id_account'
    ];

    
}
