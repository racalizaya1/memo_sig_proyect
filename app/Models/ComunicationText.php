<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComunicationText extends Model
{
    protected $table = 'comunication_text';
    protected $primaryKey ='id_comunication_text';
    public $fillable =[
        'body_text',
        'phone_client',
        'date_text',
        'status_text',
        'type_direction',
        'id_room',
        'id_account',
        'id_user'
    ];
}
