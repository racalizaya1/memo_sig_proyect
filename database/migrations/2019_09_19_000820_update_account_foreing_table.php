<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAccountForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account', function (Blueprint $table) {
            $table->foreign('id_time_zone')->references('id_time_zone')->on('time_zone');
            $table->foreign('id_comunication')->references('id_comunication')->on('comunication');
            $table->foreign('id_state')->references('id_state')->on('state');
            $table->foreign('id_country')->references('id_country')->on('country');
            $table->foreign('id_type_account')->references('id_type_account')->on('type_account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account', function (Blueprint $table) {
            $table->dropForeign(['id_time_zone']);
            $table->dropForeign(['id_comunication']);
            $table->dropForeign(['id_state']);
            $table->dropForeign(['id_country']);
            $table->dropForeign(['id_type_account']);
        });
    }
}
