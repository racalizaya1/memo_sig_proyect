<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\Table;

class UpdateIdAccountToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            
            $table->foreign('id_account')->references('id_account')->on('account');
            $table->foreign('id_type_role')->references('id_type_role')->on('type_role');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            
            $table->dropForeign(['id_account']);
            $table->dropForeign(['id_type_role']);
            
        });
    }
}
