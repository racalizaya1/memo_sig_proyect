<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental', function (Blueprint $table) {
            $table->increments('id_rental');
            $table->float('ammount', 8, 2)->nullable(true);
            $table->integer('days_rental')->nullable(true);
            $table->float('amount_by_day', 8, 2)->nullable(true);
            $table->date('date_rental')->nullable(true);
            $table->integer('id_room')->nullable(true)->unsigned();
            $table->bigInteger('id_user')->nullable(true)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental');
    }
}
