<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotificationForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification', function (Blueprint $table) {
            $table->foreign('id_account')->references('id_account')->on('account');
            $table->foreign('id_room')->references('id_room')->on('room');
            $table->foreign('id_type_notification')->references('id_type_notification')->on('type_notification');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notification', function (Blueprint $table) {
            $table->dropForeign(['id_account']);
            $table->dropForeign(['id_room']);
            $table->dropForeign(['id_type_notification']);
        });
    }
}
