<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateScoreRoomForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('score_room', function (Blueprint $table) {
            $table->foreign('id_rental')->references('id_rental')->on('rental');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('score_room', function (Blueprint $table) {
            $table->dropForeign(['id_rental']);
        });
    }
}
