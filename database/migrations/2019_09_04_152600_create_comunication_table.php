<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunication', function (Blueprint $table) {
            $table->increments('id_comunication');
            $table->string('name_account',150)->nullable(true);
            $table->string('twilio_phone',150)->nullable(true);
            $table->boolean('call_whisper')->nullable(true);
            $table->string('call_whisper_delay',150)->nullable(true);
            $table->boolean('voicemail')->nullable(true);
            $table->string('voicemail_delay',150)->nullable(true);
            $table->string('voicemail_file',150)->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunication');
    }
}
