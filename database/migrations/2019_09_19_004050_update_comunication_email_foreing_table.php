<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComunicationEmailForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comunication_email', function (Blueprint $table) {
            $table->foreign('id_room')->references('id_room')->on('room');
            $table->foreign('id_user')->references('id_user')->on('user');
            $table->foreign('id_account')->references('id_account')->on('account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comunication_email', function (Blueprint $table) {
            $table->dropForeign(['id_room']);
            $table->dropForeign(['id_user']);
            $table->dropForeign(['id_account']);
        });
    }
}
