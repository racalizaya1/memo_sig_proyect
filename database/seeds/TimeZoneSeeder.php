<?php

use Illuminate\Database\Seeder;
use App\Models\TimeZone;

class TimeZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TimeZone::class,5)->make();
    }
}
