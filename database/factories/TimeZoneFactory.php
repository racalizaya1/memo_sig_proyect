<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\TimeZone;
use Faker\Generator as Faker;

$factory->define(TimeZone::class, function (Faker $faker) {
    return [
        'value_time_zone'=> $faker->name,
        'remember_token' => Str::random(10),
    ];
});
